#include <gb/gb.h>

/******************************************************
A quick and dirty attempt at a Mandelbrot set generator
Compiler doesn't support any float emulation so it should
be fun
******************************************************/
UBYTE x, y, counter, i, j;
int x0i, x0f, y0i, y0f, xi, xf, yi, yf, xtempi, xtempf, temp1i, temp1f, temp2i, temp2f, temp3i, temp3f, normf, normi;

unsigned char spritetiles[] = {
	255,0,255,0,255,0,255,0,255,0,255,0,255,0,255,0
};

unsigned char white[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};

unsigned char black[] = {
	255, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 255, 255, 255, 255
};

unsigned char tileData[] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};

//bmpWrite simply does a tile write. The map needs to be set up as a block first
void bmpWrite(int xPos, int yPos, int xPixel, int yPixel, unsigned char value) {
	get_bkg_data(xPos + yPos*16, 1, tileData);
	if (value) {	
		tileData[yPixel*2] = tileData[yPixel*2] | (1 << (7-xPixel));
		tileData[yPixel*2+1] = tileData[yPixel*2+1] | (1 << (7-xPixel));
	}

	else {
		tileData[yPixel*2] = tileData[yPixel*2] & (~(1 << (7-xPixel)));
		tileData[yPixel*2+1] = tileData[yPixel*2+1] & (~(1 << (7-xPixel)));
	}
	set_bkg_data(xPos + yPos*16, 1, tileData);
}

//Fixed arithmetic functions. Only pass parameters within byte range
void fixedMult(int f1, int f2, int i1, int i2, int *fr, int *ir) {
	int temp;
	*ir = 0;
	temp = (f1*f2) / 128;
	if (temp == 128) {
		temp = -128;
		*ir = 1;
	}
	*fr = temp;
	temp = (i1*i2);
	*ir += temp;
	temp = i1*f2;
	*fr += temp;
	while (*fr > 127) *fr -=256;
	while (*fr < -128) *fr +=256;
	*ir += temp / 128;
	temp = i2*f1;
	*fr += temp;
	while (*fr > 127) *fr -=256;
	while (*fr < -128) *fr +=256;
	*ir += temp / 128;

	while (*ir > 127) *ir -= 256;
	while (*ir < -128) *ir += 256;
}

void fixedAdd(int f1, int f2, int i1, int i2, int *fr, int *ir) {
	*fr = f1 + f2;
	*ir = i1 + i2;
	if (*fr > 127) {
		*ir = *ir + 1;
		*fr = *fr - 256;
	}
	else if (*fr < -128) {
		*ir = *ir - 1;
		*fr = *fr + 256;
	}
	while (*ir > 127) *ir -= 256;
	while (*ir < -128) *ir += 256;
}

void fixedSub(int f1, int f2, int i1, int i2, int *fr, int *ir) {
	*fr = f1 - f2;
	*ir = i1 - i2;
	if (*fr > 127) {
		*ir = *ir + 1;
		*fr = *fr - 256;
	}
	else if (*fr < -128) {
		*ir = *ir - 1;
		*fr = *fr + 256;
	}
	while (*ir > 127) *ir -= 256;
	while (*ir < -128) *ir += 256;
}

void main() {
	disable_interrupts();
	DISPLAY_OFF;

	//Top left tile is used as a border colour
	set_bkg_data(0,1,white);
	set_bkg_data(1,1,black);

	for (i = 0; i < 16; i++) {
		for (j = 0; j < 16; j++) {
			x=i+j*16;
			set_bkg_tiles( i+2, j+1, 1, 1, &x);
		}
	}

	SHOW_BKG;
	DISPLAY_ON;
	enable_interrupts();

	for (i = 0; i < 16; i++) {
		for (j = 0; j < 16; j++) {
			for (x = 0; x < 8; x++) {
				for (y = 0; y < 8; y++) {
					x0i = 0;
					y0i = 0;
					//zoomed in config
					//x0f = (i*8 + x - 64) - 70;
					//y0f = (j*8 + y - 64) - 70;
					x0f = 2*(i*8 + x - 64);
					y0f = 2*(j*8 + y - 64);
					xi = 0;
					xf = 0;
					yi = 0;
					yf = 0;
					normi = 0;
					normf = 0;
					counter = 0;
					while (normi < 1 && counter < 50) {
						fixedMult(xf, xf, xi, xi, &temp1f, &temp1i);
						fixedMult(yf, yf, yi, yi, &temp2f, &temp2i);
						fixedSub(temp1f, temp2f, temp1i, temp2i, &temp3f, &temp3i);
						fixedAdd(temp3f, x0f, temp3i, x0i, &xtempf, &xtempi);

						fixedMult(xf, yf, xi, yi, &temp3f, &temp3i);
						fixedMult(0, temp3f, 2, temp3i, &temp1f, &temp1i);
						fixedAdd(y0f, temp1f, y0i, temp1i, &yf, &yi);

						xf = xtempf;
						xi = xtempi;

						//escape based counter						
						counter++;
						
						//calculate norm of current values to see if we're diverging
						fixedMult(xf, xf, xi, xi, &temp1f, &temp1i);
						fixedMult(yf, yf, yi, yi, &temp2f, &temp2i);
						fixedAdd(temp1f, temp2f, temp1i, temp2i, &normf, &normi);
					}

					if (counter == 50) bmpWrite(i, j, x, y, 1);
					else bmpWrite(i, j, x, y, 0);


					//fun circle routine
					/*fixedMult(x0f, x0f, x0i, x0i, &temp1f, &temp1i);
					fixedMult(y0f, y0f, y0i, y0i, &temp2f, &temp2i);
					fixedAdd(temp1f, temp2f, temp1i, temp2i, &temp3f, &temp3i);
					if (temp3i > 0) bmpWrite(i, j, x, y, 1);
					else bmpWrite(i, j, x, y, 0);*/
				}
			}
		}
	}

	while(1) {
	/* Skip four VBLs (slow down animation) */
		for(counter = 0; counter < 4; counter++){
			wait_vbl_done();
		}
		counter = joypad();

		if(counter & J_UP)
			y--;
		if(counter & J_DOWN)
			y++;
		if(counter & J_LEFT)
			x--;
		if(counter & J_RIGHT)
			x++;

		move_sprite(0,x,y);
	}
}
